import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomphoneComponent } from './customphone.component';

describe('CustomphoneComponent', () => {
  let component: CustomphoneComponent;
  let fixture: ComponentFixture<CustomphoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomphoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomphoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
