import {Injectable, OnDestroy} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {BehaviorSubject, Observable} from "rxjs";
import {catchError} from "rxjs/operators";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class PolicyDetailsService implements OnDestroy{

  constructor() {
  }

//to creat a BehaviorSubject
  private messageSource = new BehaviorSubject([]);

//to get the current message in the tunnel
  currentMessage = this.messageSource.asObservable();

  //to send a new message into the tunnel
  changeMessage(message: any) {
    this.messageSource.next(message)
  }

  ngOnDestroy(): void {
    this.messageSource.complete();
  }

}
