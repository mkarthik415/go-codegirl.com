import {Component, Input, OnInit, VERSION} from '@angular/core';
import {LoginService} from "../services/login.service";
import {PolicyDetailsService} from "../services/policy-details.service";
import {Router} from "@angular/router";
import * as _ from 'lodash';




@Component({
  selector: 'app-policyinfo',
  templateUrl: './policy-info.component.html',
  styleUrls: ['./policy-info.component.css']
})
export class PolicyInfoComponent implements OnInit {
  constructor(private loginService: LoginService,
              private policyDetailsService:PolicyDetailsService,
              private router: Router) { }

  @Input()
  policyType: string;

  account:any;
  policies:any
  infoData:any;

  ngOnInit() {
    this.loginService.currentMessage.subscribe(message => this.account = message);
    // this.policyDetailsService.currentMessage.subscribe(message => this.infoData = message);

    this.loginService.getData(this.account).subscribe(data =>{
      this.policies= data;
    });
    // this.policyDetailsService.currentMessage.subscribe(message => this.infoData = message);
  }



  protected getMoreInfo(titleData:any){
    this.policyDetailsService.changeMessage(titleData);
    this.router.navigate(['./main/policyinfo/policyDetails']);
  }

  protected getPolicyInfo(policy:any):string {
    if(!_.isNil(policy.department)){
      return policy.department === 'Miscellaneous' ? policy.policyType : policy.department;
    }
  }

}
