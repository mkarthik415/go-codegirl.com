import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PracticePolicyComponent } from './contact-info.component';

describe('PracticePolicyComponent', () => {
  let component: PracticePolicyComponent;
  let fixture: ComponentFixture<PracticePolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PracticePolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PracticePolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
