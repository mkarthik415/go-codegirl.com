export class User {
    public firstName?: string;
    public lastName?: string;
    public emailAddress?: string;
    public password?: string;
    public confirmPassword?: string;
    public areaCode?:string;
    public areaFlag?:string;
    public dob?: string;
    public phoneNumber?: number;
    public role: string = 'CU';
    public id?: number;
}
