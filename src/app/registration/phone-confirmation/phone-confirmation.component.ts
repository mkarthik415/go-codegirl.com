import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroupDirective,
  NgForm,
  FormGroup,
  FormBuilder
} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import {Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {MatSnackBar,MatSnackBarConfig,} from '@angular/material/snack-bar';
import {RegisterService} from "../../services/register.service";
import {VerifyCode} from "../../model/verifyCode";

@Component({
  selector: 'app-phone-confirmation',
  templateUrl: './phone-confirmation.component.html',
  styleUrls: ['./phone-confirmation.component.css']
})
export class PhoneConfirmationComponent implements OnInit {

  phoneForm: FormGroup;
  public inputOne:string;
  public inputTwo: string;
  public inputThree: string;
  public inputFour: string;
  public inputFive: string;
  public inputSix: string;
  public id: number;
  public sub: any;
  message: string = 'Invalid Code.';
  action: string = 'Retry';

  setAutoHide: boolean = true;
  autoHide: number = 2000;

  onInputEntry(event, id, previousId) {
    console.log(id);
    if (!event.data) {
      if (previousId) {
        document.getElementById(previousId).focus();
      }
    } else {
      if (id) {
        document.getElementById(id).focus();
      }
    }
  }

  constructor(private fb: FormBuilder, private registerService:RegisterService, private router: Router,
              private route: ActivatedRoute, private snackBar: MatSnackBar) {
    this.phoneForm = new FormGroup({
      firstRow: new FormControl(''),
      secondRow: new FormControl('', [Validators.required,  Validators.pattern('[0-9]\\d*$')]),
      thirdRow: new FormControl('', [Validators.required,  Validators.pattern('[0-9]\\d*$')]),
      fourthRow: new FormControl('', [Validators.required,  Validators.pattern('[0-9]\\d*$')]),
      fivRow: new FormControl('', [Validators.required,  Validators.pattern('[0-9]\\d*$')]),
      sixthRow: new FormControl('', [Validators.required,  Validators.pattern('[0-9]\\d*$')]),
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
    });
    console.log(this.id);
  }

  verifyCode = new VerifyCode;

  onSubmit() {
    // let config = new MatSnackBarConfig();
    console.log("enter value"+this.inputOne+this.inputTwo+this.inputThree+this.inputFour+this.inputFive+this.inputSix);


    if(this.phoneForm.valid) {

      this.verifyCode.code = this.inputOne+this.inputTwo+this.inputThree+this.inputFour+this.inputFive+this.inputSix;
      this.verifyCode.registerid = this.id;
    }
      this.registerService.confirmCode(this.verifyCode)
          .subscribe(data => {
            // login successful so redirect to return url
            if (data) {
              this.router.navigate(['./']);
            } else {
              // this.snackBar.open(this.message, this.action ? this.actionButtonLabel : undefined, config);
/*              this._snackBar.open(message, action, {
                duration: 2000,
              });*/
              console.log('invalid code');
            }
            // this.router.navigateByUrl('/phoneConfim/'+data.id);
          }, error => {
          });
    }
}
