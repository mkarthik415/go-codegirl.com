import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {BaseService} from "./base-service";
import {ActivatedRoute, Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Observable} from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class IpService extends BaseService{

  constructor(public http: HttpClient,
              public route: ActivatedRoute,
              public router: Router,public snackBar: MatSnackBar) {
    super(http,route , router, snackBar);
  }


  getData(): Observable<any> {
    return super.getData("https://cors-anywhere.herokuapp.com/http://api.ipify.org/?format=json");
  }

  getIpDetails(ip: string): Observable<any> {
    return super.getData("https://cors-anywhere.herokuapp.com/https://ipapi.co/" + ip + "/json");
  }
}
