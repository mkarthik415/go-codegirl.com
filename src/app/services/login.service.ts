import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {MatSnackBar} from '@angular/material/snack-bar';
import {BehaviorSubject, Observable, throwError} from "rxjs";
import {User} from "../model/user";
import {catchError, retry} from "rxjs/operators";
import {BaseService} from "./base-service";
import {Account} from "../model/account";


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
export class LoginService extends BaseService{


  private messageSource = new BehaviorSubject([]);
  currentMessage = this.messageSource.asObservable();

  constructor(public http: HttpClient,
              public route: ActivatedRoute,
              public router: Router,
              public snackBar: MatSnackBar) {
    super(http,route , router, snackBar);
  }

  changeMessage(message: any) {
    this.messageSource.next(message)
  }


  loginUser(account: Account): Observable<any> {
    return super.postData(this.teloswsUrl + 'api/auth/login', account);
  }

  getData(email: string): Observable<any> {
    return super.getData(this.teloswsUrl + 'api/data/byEmail?email=' + email);
  }

}
