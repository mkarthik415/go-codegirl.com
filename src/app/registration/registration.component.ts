import {Component, OnInit} from '@angular/core';
import {
    FormControl,
    FormGroupDirective,
    NgForm,
    FormGroup,
    FormBuilder
} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import {Validators} from '@angular/forms';
import {User} from "../model/user";
import {RegisterService} from "../services/register.service";
import {
    EmailValidation,
    PasswordValidation, RepeatPasswordEStateMatcher,
    RepeatPasswordValidator
} from "./validators";
import {Router, ActivatedRoute} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {IpService} from "../services/ip.service";


export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}

export interface Food {
    value: string;
    viewValue: string;
}


@Component({
    selector: 'app-registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

    selected = 'india';


    myControl = new FormControl();
    options: string[] = ['+91', '+1',];

    hide = true;
    // errorMatcher = new CrossFieldErrorMatcher();

    public registerForm: any;

    passwordsMatcher = new RepeatPasswordEStateMatcher;

    constructor(private registerService: RegisterService, private fb: FormBuilder,
                private router: Router, private snackBar: MatSnackBar, private ip: IpService) {

    }

    ipAddress: string;
    ipCountry: string;

    ngOnInit() {

        this.getIP();


        console.log("enter");
        this.registerForm = this.fb.group({
                firstName: new FormControl('', [Validators.required, Validators.maxLength(60)]),
                lastName: new FormControl('', [Validators.required, Validators.maxLength(60)]),
                emailAddress: new FormControl("", EmailValidation),
                password: new FormControl("", PasswordValidation),
                confirmPassword: new FormControl('', [Validators.required, Validators.minLength(5)]),
                dob: new FormControl('', [Validators.required,]),
                areaCode: new FormControl('', [Validators.required]),
                areaFlag: new FormControl('', [Validators.required]),
                phoneNumber: new FormControl('', [Validators.required, Validators.maxLength(10)]),


            },
            {validator: RepeatPasswordValidator});
        this.setAreaCodeValidators();

    }


    user = new User;

    onSubmit(value: any) {

        console.log('this the user name', value);
        if (this.registerForm.valid) {
            console.log('this the user name', value);
            this.user.firstName = value.firstName;
            this.user.lastName = value.lastName;
            this.user.phoneNumber = value.phoneNumber;
            this.user.areaCode = value.areaCode;
            this.user.dob = value.dob;
            this.user.password = value.password;
            this.user.emailAddress = value.emailAddress;
            this.user.role = 'CU';
        }


        console.log('this the user name', value);
        localStorage.removeItem('token');
        this.registerService.registerUser(this.user)
            .subscribe(data => {
                // login successful so redirect to return url
                console.log(data);
                if (!data.error) {
                    this.router.navigate(['/phoneCode', data.id]);
                } else {
                    this.registerService.openSnackBar('Error ' + data.error, '');
                }
                // this.router.navigateByUrl('/phoneConfim/'+data.id);
            }, error => {
                console.log('error message created' + error);
            });
    }

    public hasError = (controlName: string, errorName: string) => {
        return this.registerForm.controls[controlName].hasError(errorName);
    }


    setAreaCodeValidators() {
        const phoneNumber = this.registerForm.get('phoneNumber');

        this.registerForm.get('areaCode').valueChanges
            .subscribe(value => {
                if (value === 'india') {
                    phoneNumber.setValidators([Validators.pattern('[6-9]\\d{9}')]);
                }

                if (value === 'usa') {
                    phoneNumber.setValidators([Validators.pattern('[0-9]{3}[0-9]{3}[0-9]{4}$')]);
                }

                phoneNumber.updateValueAndValidity();
            });
    }

    getIP() {
        this.ip.getData().subscribe((res: any) => {
            this.ipAddress = res.ip;

            this.ip.getIpDetails(this.ipAddress).subscribe((res: any) => {

                this.ipCountry = res.country;
            });
        });
    }


}

