import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {BaseService} from "./base-service";
import {ActivatedRoute, Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DocumentsService extends BaseService {

  constructor(public http: HttpClient,
              public route: ActivatedRoute,
              public router: Router,public snackBar: MatSnackBar) {
    super(http,route , router, snackBar);
  }

  getDocument(id: any): Observable<any> {
    return super.getData(this.teloswsUrl  + 'api/data/documentsByClientId/?serialNumber=' + id);
  }

  getPdf(id: any,httpOptions?: any): Observable<any> {
    return super.getData(this.teloswsUrl  + 'api/data/getPdfById/?id=' + id, httpOptions);
  }
}
