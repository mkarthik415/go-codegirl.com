import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {EmailConfirmationComponent} from './email-confirmation/email-confirmation.component';
import {PhoneConfirmationComponent} from './phone-confirmation/phone-confirmation.component';
import {ContactInfoComponent} from "./contact-info/contact-info.component";
import {LoginComponent} from "../login/login.component";
import {RegistrationComponent} from "./registration.component";


@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '', component: RegistrationComponent
            },
            {
                path: 'phoneCode/:id', component: PhoneConfirmationComponent
            },
            {
                path: 'emailConfim', component: EmailConfirmationComponent
            },
            {
                path: 'contact', component: ContactInfoComponent
            }
        ])
    ],
    exports: [RouterModule]
})
export class RegistrationRoutingModule {

}
