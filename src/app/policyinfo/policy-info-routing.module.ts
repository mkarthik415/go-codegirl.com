import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import {PolicyInfoComponent} from "./policy-info.component";
import {PolicyDetailsComponent} from "./policy-details/policy-details.component";

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '', component: PolicyInfoComponent
            },
            {
                path: 'policyDetails', component: PolicyDetailsComponent
            }
        ])
    ],
    exports: [RouterModule]
})
export class PolicyInfoRoutingModule {

}
